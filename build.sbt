name := """cdlenespanol-quizzxml"""
organization := "com.aztlansoft.cdlenespanol"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)


scalaVersion := "2.13.1"

libraryDependencies += guice

// https://mvnrepository.com/artifact/commons-io/commons-io
libraryDependencies += "commons-io" % "commons-io" % "2.6"

// https://mvnrepository.com/artifact/org.apache.commons/commons-compress
libraryDependencies += "org.apache.commons" % "commons-compress" % "1.19"

libraryDependencies ++= Seq(
	"org.apache.poi" % "poi" % "3.10.1",
	"org.apache.poi" % "poi-ooxml" % "3.10.1"
)