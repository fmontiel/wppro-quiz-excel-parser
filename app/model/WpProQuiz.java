package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WpProQuiz {

	@XmlElement(name = "header")
	public Header header = new Header();

	@XmlElement(name = "data")
	public Data data = new Data();
}
