package model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import utils.AnswerText;
import utils.StortText;

public class Answer {

	@XmlAttribute
	public String points = "1";
	@XmlAttribute
	public String correct = "false";

	@XmlElement
	public AnswerText answerText = new AnswerText();

	@XmlElement
	public StortText stortText = new StortText();

}
