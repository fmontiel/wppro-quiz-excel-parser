package model;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlValue;

import utils.AdminEmail;
import utils.Forms;
import utils.QuizModus;
import utils.QuizRunOnce;
import utils.ResultText;
import utils.ShowMaxQuestion;
import utils.Statistic;
import utils.Title;
import utils.TopList;
import utils.UserEmail;

public class Quiz {

	@XmlElement(name = "title")
	public Title title = new Title(); // titleHidden="true"

	@XmlElement
	public String text = "descripcion";
	@XmlElement
	private String category = "Examen de Frenos";
	@XmlElement
	private ResultText resultText = new ResultText(); // gradeEnabled="false">
	@XmlElement
	private String btnRestartQuizHidden = "false";
	@XmlElement
	private String btnViewQuestionHidden = "true";
	@XmlElement
	private String questionRandom = "false";
	@XmlElement
	private String answerRandom = "false";
	@XmlElement
	private String timeLimit = "0";
	@XmlElement
	private String showPoints = "false";
	@XmlElement
	private Statistic statistic = new Statistic(); // activated="false" ipLock="1440"
	@XmlElement
	private QuizRunOnce quizRunOnce = new QuizRunOnce(); // type="1" cookie="false" time="0"
	@XmlElement
	private String numberedAnswer = "false";
	@XmlElement
	private String hideAnswerMessageBox = "false";
	@XmlElement
	private String disabledAnswerMark = "false";
	@XmlElement
	private ShowMaxQuestion showMaxQuestion = new ShowMaxQuestion(); // showMaxQuestionValue="1"
																		// showMaxQuestionPercent="false"
	@XmlElement
	private TopList toplist = new TopList();// activated="false"
	@XmlElement
	private String showAverageResult = "true";
	@XmlElement
	private String prerequisite = "false";
	@XmlElement
	private String showReviewQuestion = "false";
	@XmlElement
	private String quizSummaryHide = "true";
	@XmlElement
	private String skipQuestionDisabled = "true";
	@XmlElement
	private String emailNotification = "0";
	@XmlElement
	private String userEmailNotification = "false";
	@XmlElement
	private String showCategoryScore = "false";
	@XmlElement
	private String hideResultCorrectQuestion = "false";

	@XmlElement
	private String hideResultQuizTime = "true";
	@XmlElement
	private String hideResultPoints = "false";
	@XmlElement
	private String autostart = "false";
	@XmlElement
	private String forcingQuestionSolve = "true";
	@XmlElement
	private String hideQuestionPositionOverview = "true";
	@XmlElement
	private String hideQuestionNumbering = "false";
	@XmlElement
	private String sortCategories = "false";
	@XmlElement
	private String showCategory = "false";
	@XmlElement
	private QuizModus quizModus = new QuizModus(); // questionsPerPage="10"
	@XmlElement
	private String startOnlyRegisteredUser = "true";

	@XmlElement
	private AdminEmail adminEmail = new AdminEmail();

	@XmlElement
	private UserEmail userEmail = new UserEmail();

	@XmlElement
	private Forms forms = new Forms(); // activated="false" position="0"

	private List<Question> questions;


	@XmlElementWrapper
    @XmlElement(name="question")
	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	
	

}
