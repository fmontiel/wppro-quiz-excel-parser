package model;

import javax.xml.bind.annotation.XmlAttribute;

public class Header {

	@XmlAttribute(name = "version")
	private String version = "0.37";
	@XmlAttribute(name = "exportVersion")
	private String exportVersion = "1";

}
