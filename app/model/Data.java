package model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Data {

	@XmlElement(name = "quiz")
	public List<Quiz> quiz = new ArrayList<Quiz>();

}
