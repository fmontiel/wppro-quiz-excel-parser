package model;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlValue;

import utils.TipMsg;

public class Question {

	@XmlAttribute
	public String answerType = "single";
	@XmlElement
	public String title;
	@XmlElement
	public String points = "1";
	@XmlElement
	public String questionText;
	@XmlElement
	public String correctMsg = "<![CDATA[Muy bien! Tu respuesta es correcta!]]>";
	@XmlElement
	public String incorrectMsg = "<![CDATA[Revisa la respuesta correcta resaltada en verde]]>";

	@XmlElement
	public TipMsg tipMsg = new TipMsg(); // enabled="true"

	@XmlElement
	public String category; // empty

	@XmlElement
	public String correctSameText = "false";

	@XmlElement
	public String showPointsInBox = "false";

	@XmlElement
	public String answerPointsActivated = "false";

	@XmlElement
	public String answerPointsDiffModusActivated = "false";

	@XmlElement
	public String disableCorrect = "false";

	private List<Answer> answers;

	@XmlElementWrapper
	@XmlElement(name = "answer")
	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

}
