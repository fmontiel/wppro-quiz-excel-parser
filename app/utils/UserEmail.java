package utils;

import javax.xml.bind.annotation.XmlElement;

public class UserEmail {
	@XmlElement
	private String to = "-1";
	@XmlElement
	private String toUser = "false";
	@XmlElement
	private String toForm = "false";
	@XmlElement
	private String form = "";
	@XmlElement
	private String subject = "Wp-Pro-Quiz: One user completed a quiz";
	@XmlElement
	private String html = "false";
	@XmlElement
	private String message = "Wp-Pro-Quiz The user \"$username\" has completed \"$quizname\" the quiz. Points: $points Result: $result";

}
