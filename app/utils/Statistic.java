package utils;

import javax.xml.bind.annotation.XmlAttribute;

public class Statistic {
	@XmlAttribute
	private String activated = "false";
	@XmlAttribute
	private String ipLock = "1440";
}
