package utils;

import javax.xml.bind.annotation.XmlElement;

public class AdminEmail {

	@XmlElement
	private String to = "";
	@XmlElement
	private String form = "";
	@XmlElement
	private String subject = "Wp-Pro-Quiz: One user completed a quiz";
	@XmlElement
	private String html = "false";
	@XmlElement
	private String message = "Wp-Pro-Quiz The user \"$username\" has completed \"$quizname\" the quiz. Points: $points Result: $result";

}
