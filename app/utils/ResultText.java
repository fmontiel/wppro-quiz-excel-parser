package utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;


public class ResultText {

	@XmlAttribute
	private String gradeEnabled = "false";

	@XmlValue
//	private String value = "<![CDATA[]]>";
	private String value = "";
}
