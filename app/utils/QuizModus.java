package utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class QuizModus {

	@XmlAttribute
	private String questionsPerPage = "10";
	@XmlValue
	private String value = "2";
}
