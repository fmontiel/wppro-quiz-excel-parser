package utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class TipMsg {
	@XmlAttribute
	private String enabled = "true";
	@XmlValue
//	private String value = "<![CDATA[]]>";
	private String value = "";
}
