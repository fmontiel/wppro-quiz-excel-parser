package utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class AnswerText {
	@XmlAttribute
	public String html = "true";

	@XmlValue
	public String value = "";
}
