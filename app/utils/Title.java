package utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class Title {
	@XmlAttribute
	private String titleHidden = "true";

	@XmlValue
	public String value = "";
	// <![CDATA[Examen Frenos ES-EN # 3]]>
}
