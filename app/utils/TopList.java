package utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class TopList {

	@XmlAttribute
	private String activated = "false";
	
	@XmlElement
	private String toplistDataAddPermissions = "1";
	@XmlElement
	private String toplistDataSort = "1";
	@XmlElement
	private String toplistDataAddMultiple = "false";
	@XmlElement
	private String toplistDataAddBlock = "1";
	@XmlElement
	private String toplistDataShowLimit = "1";
	@XmlElement
	private String toplistDataShowIn = "0";
	@XmlElement
	private String toplistDataCaptcha = "false";
	@XmlElement
	private String toplistDataAddAutomatic = "false";

}
