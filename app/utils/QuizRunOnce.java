package utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class QuizRunOnce {
	@XmlAttribute
	private String type = "1";
	@XmlAttribute
	private String cookie = "false";
	@XmlAttribute
	private String time = "0";
	@XmlValue
	private String value = "false";
}
