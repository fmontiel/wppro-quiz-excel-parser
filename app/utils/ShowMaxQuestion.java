package utils;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class ShowMaxQuestion {

	@XmlAttribute
	private String showMaxQuestionValue = "1";
	@XmlAttribute
	private String showMaxQuestionPercent = "false";

	@XmlValue
	private String value = "false";
}
