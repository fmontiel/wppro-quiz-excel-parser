package controllers;

import play.libs.Json;
import play.mvc.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Http.Response;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.databind.node.ObjectNode;

import model.Quiz;
import model.WpProQuiz;

/**
 * This controller contains an action to handle HTTP requests to the
 * application's home page.
 */
public class HomeController extends Controller {

	
	boolean modeZIP = false;
	/**
	 * An action that renders an HTML page with a welcome message. The configuration
	 * in the <code>routes</code> file means that this method will be called when
	 * the application receives a <code>GET</code> request with a path of
	 * <code>/</code>.
	 */

//	<?xml version="1.0" encoding="utf-8"?>
//	<wpProQuiz>
//	    <header version="0.37" exportVersion="1"/>
//	    <data>
//	        <quiz>

	public Result index() {
		WpProQuiz quiz = new WpProQuiz();

		// Method which uses JAXB to convert object to XML
		String result = jaxbObjectToXML(quiz);
		// response().setHeader("Co", value);

		return ok(result).as("text/xml");
	}

	public Result generateQuizXMLAsZip() {
		MultipartFormData<File> body = request().body().asMultipartFormData();
		ObjectNode result = Json.newObject();
		String msg = "";
		if (body != null) {
			FilePart<File> excel = body.getFile("file");
			if (excel != null && !excel.getFilename().trim().isEmpty()) {
				File file = excel.getFile();
				List<Quiz> list = ImportController.generateQuizzesFromFile(file);
				WpProQuiz proQuiz = new WpProQuiz();
				proQuiz.data.quiz = list;

				if (!modeZIP) {
					String resultXML = jaxbObjectToXML(proQuiz);
					response().setHeader(Response.CONTENT_DISPOSITION, "attachment; filename=\""  + excel.getFilename() + ".xml\"");
					return ok(resultXML).as("text/xml");
				} else {

					// FILE - ZIP
					File fileDestination = new File("GeneratedZip.zip");
					String nameDestination = "xmls" + new Date().getTime();
					File directory = new File(nameDestination);
					boolean bool = directory.mkdir();
					if (bool) {
						System.out.println("Directory XMLS created successfully");
					} else {
						System.out.println("Sorry couldn’t create XMLS directory");
					}
					for (Quiz quiz : list) {
						File fileSource = new File(nameDestination + "/source" + new Date().getTime() + ".xml");
						BufferedWriter writer;
						try {
							fileSource.createNewFile();

							writer = new BufferedWriter(new FileWriter(fileSource));

							String resultXML = jaxbObjectToXML(proQuiz);
							writer.write(resultXML);

							writer.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					try {
						addFilesToZip(directory, fileDestination);
					} catch (IOException | ArchiveException e) {
						e.printStackTrace();
					}
					response().setHeader(Response.CONTENT_DISPOSITION, "attachment; filename=\"filename.zip\"");
					response().setHeader(Response.CONTENT_TYPE, "application/zip");
					return ok(fileDestination);
				}
			}
		}
		msg = "Archivo faltante";
		result.put("message", msg);
		result.put("respond", 0);
		return ok(result);
	}

	private static String jaxbObjectToXML(WpProQuiz employee) {
		String xmlContent = "";
		try {
			// Create JAXB Context
			JAXBContext jaxbContext = JAXBContext.newInstance(WpProQuiz.class);

			// Create Marshaller
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// Required formatting??
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			// jaxbMarshaller.setProperty("jaxb.encoding", "Unicode");
			// jaxbMarshaller.setProperty(CharacterEscapeHandler.class.getName(), new
			// CustomCharacterEscapeHandler());

			jaxbMarshaller.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, "utf-8");
			// Print XML String to Console
			StringWriter sw = new StringWriter();

			// Write XML to StringWriter
			jaxbMarshaller.marshal(employee, sw);

			// Verify XML Content
			System.out.println(xmlContent);
			xmlContent = sw.toString();

		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return xmlContent;

	}

	/**
	 * Add all files from the source directory to the destination zip file
	 *
	 * @param source      the directory with files to add
	 * @param destination the zip file that should contain the files
	 * @throws IOException      if the io fails
	 * @throws ArchiveException if creating or adding to the archive fails
	 */
	private void addFilesToZip(File source, File destination) throws IOException, ArchiveException {
		OutputStream archiveStream = new FileOutputStream(destination);
		ArchiveOutputStream archive = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.ZIP,
				archiveStream);

		Collection<File> fileList = FileUtils.listFiles(source, null, true);

		for (File file : fileList) {
			String entryName = getEntryName(source, file);
			ZipArchiveEntry entry = new ZipArchiveEntry(entryName);
			archive.putArchiveEntry(entry);

			BufferedInputStream input = new BufferedInputStream(new FileInputStream(file));

			IOUtils.copy(input, archive);
			input.close();
			archive.closeArchiveEntry();
		}

		archive.finish();
		archiveStream.close();
	}

	/**
	 * Remove the leading part of each entry that contains the source directory name
	 *
	 * @param source the directory where the file entry is found
	 * @param file   the file that is about to be added
	 * @return the name of an archive entry
	 * @throws IOException if the io fails
	 */
	private String getEntryName(File source, File file) throws IOException {
		int index = source.getAbsolutePath().length() + 1;
		String path = file.getCanonicalPath();

		return path.substring(index);
	}

}
