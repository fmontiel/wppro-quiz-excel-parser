package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;

import model.Answer;
import model.Question;
import model.Quiz;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import utils.AnswerText;

import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class ImportController extends Controller {

	@Inject
	private play.data.FormFactory formFactory;

	private static String getStringFromCell(Cell cell) {
		DataFormatter df = new DataFormatter();
		String value = df.formatCellValue(cell);
		return value.trim();
	}

	public static List<Quiz> generateQuizzesFromFile(File file) {
		int countNuevos = 0;
		int countUpdates = 0;
		int countErrors = 0;

		List<Quiz> quizzes = new ArrayList<>();

		try {
			FileInputStream fileIS = new FileInputStream(file);
			// Get the workbook instance for XLS file
			Workbook wb = null;
			try {
				wb = WorkbookFactory.create(file);
			} catch (EncryptedDocumentException e1) {
				e1.printStackTrace();
			} catch (InvalidFormatException e1) {
				e1.printStackTrace();
			}
			List<String> sheetNames = new ArrayList<>();
			List<Iterator<Row>> sheetsRowIterator = new ArrayList<>();
			int numberSheets = 0;
			if (wb instanceof HSSFWorkbook) {
				HSSFWorkbook workbook = new HSSFWorkbook(fileIS);
				HSSFSheet sheet = workbook.getSheetAt(0);
				numberSheets = workbook.getNumberOfSheets();
				for (int i = 0; i < numberSheets; i++) {
					sheet = workbook.getSheetAt(i);
					sheetNames.add(sheet.getSheetName());
					sheetsRowIterator.add(sheet.rowIterator());
				}
				// rowIterator = sheet.iterator();
			} else if (wb instanceof SXSSFWorkbook) {
				XSSFWorkbook xs = new XSSFWorkbook(fileIS);
				SXSSFWorkbook workbook = new SXSSFWorkbook(xs);
				Sheet sheet = workbook.getSheetAt(0);
				// rowIterator = sheet.iterator();
				numberSheets = workbook.getNumberOfSheets();
				for (int i = 0; i < numberSheets; i++) {
					sheet = workbook.getSheetAt(i);
					sheetNames.add(sheet.getSheetName());
					sheetsRowIterator.add(sheet.rowIterator());
				}
			} else if (wb instanceof XSSFWorkbook) {
				XSSFWorkbook workbook = new XSSFWorkbook(fileIS);
				XSSFSheet sheet = workbook.getSheetAt(0);
				numberSheets = workbook.getNumberOfSheets();
				// rowIterator = sheet.iterator();
				for (int i = 0; i < numberSheets; i++) {
					sheet = workbook.getSheetAt(i);
					sheetNames.add(sheet.getSheetName());
					sheetsRowIterator.add(sheet.rowIterator());
				}
			}
			Cell cell = null;
			Row row = null;

			for (int j = 0; j < sheetsRowIterator.size(); j++) {
				Logger.info("Sheet : " + sheetNames.get(j) + " ======================= ");
				Iterator<Row> rowIterator = sheetsRowIterator.get(j);
				row = rowIterator.next(); // JUMP HEADERS

				Logger.info("Row : " + row.getRowNum());

				Iterator<Cell> cellIterator = row.cellIterator();
				Quiz quiz = new Quiz();

				quiz.title.value = sheetNames.get(j);
				quiz.text = sheetNames.get(j);
				
				quiz.setQuestions(new ArrayList<>());

				while (rowIterator.hasNext()) {
					row = rowIterator.next();
					Logger.info("Row : " + row.getRowNum());
					cellIterator = row.cellIterator();
					cell = cellIterator.next();

					String originalQuestion = getStringFromCell(cell);
					if (originalQuestion.trim().isEmpty()) {
						continue;
					}
					Logger.info("Question:");
					Logger.info(originalQuestion);
					String[] questionLangs = splitText2Langs(originalQuestion);
					String question1 = questionLangs[0];
					String question2 = questionLangs[1];

					Question question = new Question();
					int maxSizeTitle = 50;
					if (question1.length() < maxSizeTitle) {
						maxSizeTitle = question1.length();
					}
					question.title = "" + question1.substring(0, maxSizeTitle);
					question.questionText = "<p>" + question1 + "</p>" + "<b><p>" + question2 + "</p></b>";

					// A
					cell = cellIterator.next();
					String answerAOriginalText = getStringFromCell(cell);
					Answer a = new Answer();
					String[] answerALangs = splitText2Langs(answerAOriginalText);
					String valueA = ""; 
					if(answerALangs == null) { //SAME ANSWER
						valueA = answerAOriginalText;
					}else {
						valueA = answerALangs[0] + "<br><b>" + answerALangs[1] + "</b>";
					}
					AnswerText atA = new AnswerText();
					atA.value = valueA;
					a.answerText = atA;
					// B
					cell = cellIterator.next();
					String answerBOriginalText = getStringFromCell(cell);
					Answer b = new Answer();
					String[] answerBLangs = splitText2Langs(answerBOriginalText);
					String valueB = ""; 
					if(answerBLangs == null) { //SAME ANSWER
						valueB = answerBOriginalText;
					}else {
						valueB = answerBLangs[0] + "<br><b>" + answerBLangs[1] + "</b>";
					}
					AnswerText atB = new AnswerText();
					atB.value = valueB;
					b.answerText = atB;

					// C
					cell = cellIterator.next();
					String answerCOriginalText = getStringFromCell(cell);
					Answer c = new Answer();
					String[] answerCLangs = splitText2Langs(answerCOriginalText);
					String valueC = ""; 
					if(answerCLangs == null) { //SAME ANSWER
						valueC = answerCOriginalText;
					}else {
						valueC = answerCLangs[0] + "<br><b>" + answerCLangs[1] + "</b>";
					}
					AnswerText atC = new AnswerText();
					atC.value = valueC;
					c.answerText = atC;

					// Answer correct
					cell = cellIterator.next();
					String answerCorrect = getStringFromCell(cell);
					switch (answerCorrect) {
					case "A":
						a.correct = "true";
						break;
					case "B":
						b.correct = "true";
						break;
					case "C":
						c.correct = "true";
						break;
					}

					// Instruction
					cell = cellIterator.next();
					String correctMsg = getStringFromCell(cell);

					// IncorrectAnswerMessage
					cell = cellIterator.next();
					String incorrectMsg = getStringFromCell(cell);

					question.correctMsg = correctMsg;
					question.incorrectMsg = incorrectMsg;

					List<Answer> answers = new ArrayList<>();
					answers.add(a);
					answers.add(b);
					answers.add(c);

					question.setAnswers(answers);
					quiz.getQuestions().add(question);
				}
				quizzes.add(quiz);
			}

			fileIS.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}

		return quizzes;
	}

	private static String[] splitText2Langs(String text) {
		try {

			String question1 = text.split("\\*")[2];
			question1 = question1.replace("\n", "").trim();
			String question2 = text.split("\\*")[5];
			question2 = question2.replace("\n", "").trim();
			Logger.info("Text1: " + question1);
			Logger.info("Text2: " + question2);
			String[] strings = new String[] { question1, question2 };
			return strings;
		} catch (Exception e) {
		}
		return null;
	}

}
